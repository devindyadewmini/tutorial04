#include<stdio.h>

//Funtions
int armstrong(int a);

int armstrong(int a)
{
    int ld = 0, power = 0, sum = 0;
    int n = a;

  while(n!=0)
    {
     ld = n % 10;
     power = ld*ld*ld;
     sum =sum + power;
     n /= 10;
    }

  if(sum == a)
    printf("\n%d is a ARMSTRONG NUMBER!\n",a);
  else
    printf("\n%d is NOT AN ARMSTRONG NUMBER!\n",a);
}

void main()
{
    int a;
    printf("Enter the Number : ");
    scanf("%d" , &a);

    armstrong(a);

    return;
}
