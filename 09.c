#include<stdio.h>

//Funtions
int armstrong(int a);

int armstrong(int a)
{
    int ld = 0, power = 0, sum = 0,pass;
    int n = a;

  while(n!=0)
    {
     ld = n % 10;
     power = ld*ld*ld;
     sum =sum + power;
     n /= 10;
    }

    if(sum == a)
        pass = 1;
    else
        pass = 0;

    return pass;
}

int main()
{
    int n1,n2,i,f;
    printf("Enter the Starting Value >>>  ");
    scanf("%d",&n1);
    printf("Enter the Ending Value >>>  ");
    scanf("%d",&n2);
    printf("\nArmstrong numbers between %d and %d are >>> \n", n1, n2);


    for(i=n1;i<=n2;++i)
   {
      f=armstrong(i);
      if(f==1)
         printf("\n%d",i);
   }
    printf("\n\n");
    return 0;
}
