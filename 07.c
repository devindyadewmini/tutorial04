#include<stdio.h>

int printprime(int z)
{
   int j,f=0;
   for(j=2;j<=z/2;++j){
        if(z%j==0){
            f=1;
            break;
        }
   }
   return f;
}

int main()
{
    int n1,n2,i,f;
    printf("Enter the Starting Value >>>  ");
    scanf("%d",&n1);
    printf("Enter the Ending Value >>>  ");
    scanf("%d",&n2);
    printf("\nPrime numbers between %d and %d are >>> \n\n", n1, n2);


    for(i=n1;i<=n2;++i)
   {
      f=printprime(i);
      if(f==0)
         printf("%d, ",i);
   }
    printf("\n\n");
    return 0;
}
