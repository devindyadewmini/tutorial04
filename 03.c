#include<stdio.h>

//Funtions
int maximum(int n1,int n2);
int minimum(int n1,int n2);

int maximum(int n1,int n2)
{
    int max;
    if (n1 > n2)
        max = n1;
    else
        max = n2;

    return max;
}

int minimum(int n1,int n2)
{
    int min;
    if (n1 < n2)
        min = n1;
    else
        min = n2;

    return min;
}

int main()
{
    int n1,n2;
    printf("\nEnter First Number >>>  ");
    scanf("%d",&n1);
    printf("\nEnter Second Number >>>  ");
    scanf("%d",&n2);

    printf("\n\n%d is the MAXIMUM NUMBER! \n",maximum(n1,n2));
    printf("%d is the MINIMUM NUMBER! \n\n",minimum(n1,n2));

    return 0;
}
