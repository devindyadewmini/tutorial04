#include<stdio.h>

int strong(int n)
{
    int q,rem,fact=1,i,result=0,pass;
    q=n;
        while (q!=0)
        {
            rem=q%10;
            for (i=1; i<=rem; i=i+1)
            {
                fact = fact * i;
            }
            result = result + fact;
            fact = 1;
            q=q/10;
        }
    if (result==n)
        pass = 1;
    else
        pass = 0;

    return pass;
}

int main()
{
    int n1,n2,i,f;
    printf("Enter the Starting Value >>>  ");
    scanf("%d",&n1);
    printf("Enter the Ending Value >>>  ");
    scanf("%d",&n2);
    printf("\nStrong numbers between %d and %d are >>> \n", n1, n2);


    for(i=n1;i<=n2;++i)
   {
      f=strong(i);
      if(f==1)
         printf("\n%d",i);
   }
    printf("\n\n");
    return 0;
}
