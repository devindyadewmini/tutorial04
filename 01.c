#include<stdio.h>

//Funtions
int cube(int N);

int cube(int N)
{
    return N * N * N;
}

int main()
{
    int num;
    printf("\nEnter the Number you want to get cube value >>>  ");
    scanf("%d",&num);

    printf("\nCube of %d is =  %d \n \n \n",num,cube(num));

    return 0;
}
