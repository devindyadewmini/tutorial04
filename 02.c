#include <stdio.h>

#define PI 3.14

//Funtions
double diameter(double r);
double circumference(double r);
double area(double r);

double diameter(double r)
{
    return 2 * r ;
}

double circumference(double r)
{
    return 2 * PI * r ;
}

double area(double r)
{
    return PI * r * r ;
}

int main()
{
    double r;
    printf("Enter the radius of circle >>> ");
    scanf("%lf",&r);
        printf("\n\nDiameter of %.2lf radius circle is = %.2lf \n \n",r,diameter(r));
        printf("Circumference of %.2lf radius circle is = %.2lf \n \n",r,circumference(r));
        printf("Area of %.2lf radius circle is = %.2lf \n \n",r,area(r));

    return 0;
}

